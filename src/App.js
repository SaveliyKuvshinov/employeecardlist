import React from 'react';
import './App.css';
import EmployeeCardList from "./components/EmployeeCardList";
import Header from "./components/Header";

function App() {
  return (
    <div className="App">
        <Header/>
        <EmployeeCardList/>
    </div>
  );
}

export default App;
