import React, { Component } from 'react';
import { DocumentCard } from 'office-ui-fabric-react/lib/DocumentCard';
import {Persona, PersonaSize} from 'office-ui-fabric-react/lib/Persona';
import '../App.css';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Icon } from '@fluentui/react/lib/Icon';
import { initializeIcons } from '@uifabric/icons';

initializeIcons();

class EmployeeCard extends Component {
    getPersonaData() {
        const {personData} = this.props;
        return {
            imageInitials: personData.fullName.substr(0, 2),
            text: personData.fullName,
            secondaryText: personData.position
        }
    }

    editCard = () => {
        this.props.setDialogData(true, this.props.personData);
    };

    removeCard = () => {
        this.props.removeCard(this.props.personData);
    };

    getFormattedPhoneString = () => {
        const phone = this.props.personData.phone;

        return "+7 (" + phone.substr(0,3) + ") " + phone.substr(3,3) + "-" + phone.substr(6,2) + "-" + phone.substr(8,2);;
    };

    render() {
        return(
            <DocumentCard className="Document-card" style={{justifyContent: 'space-between'}}>
                <Persona
                    {...this.getPersonaData()}
                    size={PersonaSize.size64}
                />
                <div className="flex-row" style={{justifyContent: 'space-between', alignItems: 'flex-end'}}>
                    <div className="flex-column">
                        {this.props.personData.roomNumber && <Text>Кабинет №{this.props.personData.roomNumber}</Text>}
                        {this.props.personData.phone && <div className="flex-row">
                            <Icon iconName="Phone" style={{paddingRight: 10}}/>
                            <Text>{this.getFormattedPhoneString()}</Text>
                        </div>}
                        <div className="flex-row">
                            <Icon iconName="Mail" style={{paddingRight: 10}}/>
                            <Text>{this.props.personData.email}</Text>
                        </div>
                    </div>
                    <div className="flex-row">
                        <Icon iconName="Edit" onClick={this.editCard}/>
                        <Icon iconName="Cancel" onClick={this.removeCard}/>
                    </div>
                </div>
            </DocumentCard>
        )
    }
}

export default EmployeeCard;