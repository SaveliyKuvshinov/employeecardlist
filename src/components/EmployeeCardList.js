import React, { Component } from 'react';
import '../App.css';
import EmployeeCard from "./EmployeeCard";
import NewEmployeeButton from "./NewEmployeeButton";
import EmployeeDialog from "./EmployeeDialog";

const people = [
    {
        fullName: 'Иванов Иван Иванович',
        position: 'Дровосек',
        phone: '9179999999',
        email: 'admin@logicless.com',
        roomNumber: 12
    },
    {
        fullName: 'Сидоров Сидор Сидорович',
        position: 'Дояр',
        phone: '9178888888',
        email: 'info@logicless.com',
        roomNumber: 13
    }
];

class EmployeeCardList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            people: [],
            showDialog: false,
            currentPersonData: {},
            currentPersonIndex: -1
        }
    }

    componentWillMount() {
        this.setState({people: people})
    }

    setDialogData = (visible, personData = null) => {
        this.setState({ showDialog: visible, currentPersonData: personData})
    };

    addNewEmployee = (data) => {
        this.setState((prevState) => { return {people: [...prevState.people, data] }})
    };

    editEmployeeData = (data) => {
        this.setState((prevState) => {
            let newPeople = prevState.people;
            newPeople[prevState.people.findIndex((element) => element === prevState.currentPersonData)] = data;
            return { people: newPeople }
        });
    };

    removeEmployeeCard = (data) => {
        this.setState((prevState) => {
            return { people: prevState.people.filter((element) => element !== data)}
        })
    };

    compareByFullName = (x, y) => {
        return x.fullName.localeCompare(y.fullName);
    };

    render() {
        return(
            <div className="Document-card-list">
                {this.state.people.sort(this.compareByFullName).map(
                    value => <EmployeeCard personData={value} setDialogData={this.setDialogData} removeCard={this.removeEmployeeCard}/>
                    )}
                <NewEmployeeButton setDialogData={this.setDialogData}/>
                <EmployeeDialog
                    personData={this.state.currentPersonData}
                    showDialog={this.state.showDialog}
                    addNewEmployee={this.addNewEmployee}
                    editEmployeeData={this.editEmployeeData}
                    setDialogData={this.setDialogData}
                />
            </div>
        )
    }
}

export default EmployeeCardList;