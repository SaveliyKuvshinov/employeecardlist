import React, { Component } from 'react';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Text } from 'office-ui-fabric-react/lib/Text';
import '../App.css';


class EmployeeDialog extends Component {
    constructor(props) {
        super(props);

        this.editing = false;
        this.state = {
            personData: {
                fullName: '',
                position: '',
                roomNumber: '',
                phone: '',
                email: ''
            },
            formIsValid: true,
            formErrors: []
        }
    }

    toggleHideDialog = () => {
        this.props.setDialogData(false, this.state.personData);
    };

    clearData = () => {
        const emptyData = {
            fullName: '',
            position: '',
            roomNumber: '',
            phone: '',
            email: ''
        };

        this.editing = false;
        this.setState({personData: emptyData, formIsValid: true, formErrors: []});
    };

    fillEditData = data => {
        this.setState({personData: data});
    };

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (this.props.personData !== nextProps.personData && nextProps.showDialog) {
            if (nextProps.personData) {
                this.editing = true;
                this.fillEditData(nextProps.personData);
            }
            else {
                this.editing = false;
                this.clearData();
            }
        }
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState((prevState) => {
            let newData = prevState.personData;
            newData[name] = value;
            return newData;
        });
    };

    validateSubmitData = data => {
        let errors = [];
        const phoneRule = /^\d{10}$/;
        const emailRule = /\S+@\S+\.\S+/;

        if (data.phone.length > 0 && !phoneRule.test(data.phone)) {
            errors.push('Поле телефонного номера должно содержать 10 цифр');
        }
        if (!emailRule.test(data.email)) {
            errors.push('Поле email должно иметь вид *@*.*');
        }

        return errors;
    };

    handleFormSubmit = event => {
        event.preventDefault();
        let {addNewEmployee, editEmployeeData, setDialogData} = this.props;

        const errors = this.validateSubmitData(this.state.personData);
        if (errors.length > 0) {
            this.setState({formIsValid: false, formErrors: errors});
        }
        else {
            this.editing ? editEmployeeData(this.state.personData) : addNewEmployee(this.state.personData);
            setDialogData(false);
            this.clearData();
        }

    };

    render() {
        return (
            <Dialog
                dialogContentProps={{
                    type: DialogType.largeHeader,
                    title: this.editing ? 'Редактирование' : 'Новый сотрудник'
                }}
                modalProps={{ isBlocking: false }}
                hidden={!this.props.showDialog}
            >
                <form onSubmit={this.handleFormSubmit}>
                    {!this.state.formIsValid && <Text className="Form-error-text" block>Введите корректные данные:</Text>}
                    {this.state.formErrors.map((value) => <Text className="Form-error-text" block>{value}</Text>)}
                    <TextField label="ФИО" name="fullName" defaultValue={this.state.personData.fullName}  onChange={this.handleInput} required />
                    <TextField label="Должность" name="position" defaultValue={this.state.personData.position}  onChange={this.handleInput} required/>
                    <TextField label="Кабинет" name="roomNumber" defaultValue={this.state.personData.roomNumber}  onChange={this.handleInput}/>
                    <TextField label="Телефон" name="phone" defaultValue={this.state.personData.phone} onChange={this.handleInput} prefix="+7"/>
                    <TextField label="Email" name="email" defaultValue={this.state.personData.email}  onChange={this.handleInput} required/>
                    <DialogFooter>
                        <PrimaryButton type="submit" text={this.editing ? "Сохранить" : "Добавить"} disabled={this.validPhone} />
                        <DefaultButton onClick={this.toggleHideDialog} text="Отмена" />
                    </DialogFooter>
                </form>
            </Dialog>
        );
    }
}

export default EmployeeDialog;