import React, { Component } from 'react';
import { DocumentCard } from 'office-ui-fabric-react/lib/DocumentCard';
import '../App.css';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Icon } from '@fluentui/react/lib/Icon';

class NewEmployeeButton extends Component {
    showDialog = () => {
        this.props.setDialogData(true);
    };

    render() {
        return(
            <DocumentCard className="Document-card" style={{justifyContent: 'space-around', alignItems: "center"}} onClick={this.showDialog}>
                <Icon style={{height: 100, width: 100, fontSize: 100, color: "grey"}} iconName="AddFriend"/>
                <Text>Добавить сотрудника</Text>
            </DocumentCard>
        )
    }
}

export default NewEmployeeButton;