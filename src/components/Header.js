import React, { Component } from 'react';
import { Icon } from '@fluentui/react/lib/Icon';
import { Text } from 'office-ui-fabric-react/lib/Text';
import '../App.css';

class Header extends Component {
    render() {
        return (
            <header className="App-header">
                <Icon iconName='Group' style={{paddingRight: 10}}/>
                <Text variant={'large'}>Список сотрудников</Text>
            </header>
        );
    }
}

export default Header;